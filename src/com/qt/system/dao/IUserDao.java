package com.qt.system.dao;

import com.bamboo.framework.base.IDAO;
import com.bamboo.framework.entity.PageInfo;
import com.bamboo.framework.exception.SysException;
import com.qt.system.entity.UserMVO;

public interface IUserDao extends IDAO<UserMVO> {

	public PageInfo queryPage(UserMVO entity, PageInfo pageInfo) throws SysException;
	
	public UserMVO login(UserMVO entity) throws SysException;
}
