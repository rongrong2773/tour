package com.qt.system.service.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bamboo.framework.base.impl.BaseService;
import com.bamboo.framework.common.util.DateUtil;
import com.bamboo.framework.entity.PageInfo;
import com.bamboo.framework.exception.AppException;
import com.bamboo.framework.exception.SysException;
import com.qt.base.common.HttpHelper;
import com.qt.base.common.SessionUser;
import com.qt.system.dao.IUserDao;
import com.qt.system.entity.UserMVO;
import com.qt.system.service.IUserService;
import static com.qt.base.common.ApiUtil.throwAppException;

@Service
public class UserServiceImpl extends BaseService implements IUserService  {
	
	@Autowired
	private IUserDao userDao;

	@Override
	public UserMVO insert(UserMVO entity) throws SysException, AppException {
		entity.setCreateTime(DateUtil.nowTime());
		//entity.setSts(STS_NORMAL);
		return userDao.insert(entity);
	}

	@Override
	public int update(UserMVO entity) throws SysException, AppException {
		entity.setUpdateTime(DateUtil.nowTime());
		return userDao.update(entity);
	}

	@Override
	public int delete(UserMVO entity) throws SysException, AppException {
		return userDao.delete(entity);
	}

	@Override
	public List<UserMVO> queryList(UserMVO entity) throws SysException, AppException {
		return userDao.queryList(entity);
	}

	@Override
	public UserMVO queryBean(UserMVO entity) throws SysException, AppException {
		return userDao.queryBean(entity);
	}

	@Override
	public PageInfo queryPage(UserMVO entity, PageInfo pageInfo) throws SysException, AppException {
		return userDao.queryPage(entity, pageInfo);
	}

	@Override
	public boolean login(UserMVO entity) throws SysException, AppException {
		UserMVO user = userDao.login(entity);
		//判断账号密码是否正确
		throwAppException(user == null || !StringUtils.equals(entity.getPassword(), user.getPassword()), "账号或密码错误");
		//正确，存入回话
		HttpHelper.getHttpSession().setAttribute(SessionUser.SESSION_USER, user);
		return true;
	}

}
