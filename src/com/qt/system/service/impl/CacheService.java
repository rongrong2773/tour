package com.qt.system.service.impl;

import org.apache.commons.lang.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.bamboo.framework.aop.CacheRefreshPoint;
import com.bamboo.framework.cache.CacheManager;

@Service
public class CacheService implements CacheRefreshPoint {
	private static Logger logger = LoggerFactory.getLogger(CacheService.class);
	
	@Override
	public void refreshCache(ProceedingJoinPoint joinPoint, String[] cacheNames) {
		if (cacheNames != null && cacheNames.length > 0) {
			for (String name : cacheNames) {
				if (StringUtils.isBlank(name)) {
					continue;
				}
				logger.debug("\n开始刷新缓存：" + name);
				CacheManager.getInstance().reInitialize(name);
			}
		}
	}

}