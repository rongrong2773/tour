package com.qt.system.service;

import java.util.List;

import javax.jws.WebService;

import com.bamboo.framework.base.IService;
import com.bamboo.framework.entity.PageInfo;
import com.bamboo.framework.exception.AppException;
import com.bamboo.framework.exception.SysException;
import com.qt.system.entity.UserMVO;

@WebService
public interface IUserService extends IService {

	public UserMVO insert(UserMVO entity) throws SysException, AppException;
	
	public int update(UserMVO entity) throws SysException, AppException;
	
	public int delete(UserMVO entity) throws SysException, AppException;
	
	public List<UserMVO> queryList(UserMVO entity) throws SysException, AppException;
	
	public UserMVO queryBean(UserMVO entity) throws SysException, AppException;
	
	public PageInfo queryPage(UserMVO entity, PageInfo pageInfo) throws SysException, AppException;
	
	public boolean login(UserMVO entity) throws SysException, AppException;
}
