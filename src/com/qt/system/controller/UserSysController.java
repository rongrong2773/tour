package com.qt.system.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bamboo.framework.entity.PageInfo;
import com.qt.base.controller.BaseController;
import com.qt.system.entity.UserMVO;
import com.qt.system.service.IUserService;

@Controller
@RequestMapping("/system")
public class UserSysController extends BaseController {
	private static Logger logger = LoggerFactory.getLogger(UserSysController.class);
	
	@Autowired
	private IUserService userService;
	
	@RequestMapping
	public String gotoLogin(){
		return "system/login";
	}
	
	@RequestMapping(value="/gotoHome")
	public String gotoHome(){
		return "system/home";
	}
	
	/**
	 * 添加会员账号
	 * @param UserMVO
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/addUser", method=RequestMethod.POST)
	public String addUser(UserMVO entity) {
		try {
			userService.insert(entity);
		} catch (Exception e) {
			return callbackException(logger, "添加出错", e);
		}
		return callback(true, "添加成功");
	}
	
	/**
	 * 登录
	 * @param UserMVO
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public String login(UserMVO entity){
		boolean result = false;
		try {
			result = userService.login(entity);
		} catch (Exception e) {
			return callbackException(logger, "登陆出错", e);
		}
		return callback(result, "登陆成功");
	}
	
	/**
	 * 获取会员信息列表 分页
	 */
	@ResponseBody
	@RequestMapping(value="/getUserPage", method=RequestMethod.POST)
	public String getUserPage(UserMVO user) {
		try {
			PageInfo pageInfo = getPage();
			pageInfo = userService.queryPage(user, pageInfo);
			return callback(true, "查询成功", pageInfo);
		} catch (Exception e) {
			return callbackException(logger, "查询出错", e);
		}
	}
	
	/**
	 * 获取会员信息列表 
	 */
	@ResponseBody
	@RequestMapping(value="/getUserList", method=RequestMethod.POST)
	public String getUserList(UserMVO user) {
		try {
			List<UserMVO> list = userService.queryList(user);
			return callback(true, "查询成功", list);
		} catch (Exception e) {
			return callbackException(logger, "查询出错", e);
		}
	}
	
	/**
	 * 更新
	 * @param UserMVO
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/updateUser", method=RequestMethod.POST)
	public String updateUser(UserMVO entity) {
		try {
			userService.update(entity);
			return callback(true, "更新成功");
		} catch (Exception e) {
			return callbackException(logger, "更新出错", e);
		}
	}
	
	/**
	 * 删除
	 * @param UserMVO
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/deleteUser", method=RequestMethod.POST)
	public String deleteUser(UserMVO entity) {
		try {
			userService.delete(entity);
			return callback(true, "删除成功");
		} catch (Exception e) {
			return callbackException(logger, "删除出错", e);
		}
	}
}
