package com.qt.base.common;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TimeUtils {

	public static String nowTimeSdf(long time) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		return sdf.format(time);
	}

	public static String addMinuteTimeSdf(long time, int addtime) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, addtime);
		return sdf.format(calendar.getTime());
	}

	/**
	 * 将yyyy-MM-dd HH:mm:ss 转化为 yyyyMMddHHmmss
	 */
	public static String timeReplace(String time) {
		return time.replace("-", "").replace(":", "").replace(" ", "");
	}
}
