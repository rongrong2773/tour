package com.qt.base.common;

import javax.servlet.http.HttpSession;

import com.qt.system.entity.UserMVO;

/**
 * 保存用户会话
 *
 * @author yzy 2017年3月19日
 */
public class SessionUser {

	public static final String SESSION_USER = "SESSION_USER";
	public static final String USERNAME = "SESSION_USER.USERNAME";
	public static final String USER_ID = "SESSION_USER.USER_ID";
	public static final String NICKNAME = "SESSION_USER.NICKNAME";

	public static String get(String key) {
		String val = null;

		HttpSession session = HttpHelper.getHttpSession();
		UserMVO user = (UserMVO) session.getAttribute(SESSION_USER);
		switch (key) {
		case USERNAME:
			val = user.getUsername();
			break;
		case USER_ID:
			val = user.getUserId();
			break;
		case NICKNAME:
			val = user.getNickname();
			break;
		default:
			break;
		}
		return val;
	}

	public static boolean isLogined() {
		UserMVO user = (UserMVO) HttpHelper.getHttpSession().getAttribute(SESSION_USER);
		return user != null;
	}

}
