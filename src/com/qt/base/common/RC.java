package com.qt.base.common;

public enum RC {

	SUCCESS("1", "success"),

	// 账号操作类
	// 登录 11
	LOGIN_USERNAME_PASSWORD_CANNOT_NULL("1101", "用户名或密码不能为空"), LOGIN_USERNAME_PASSWORD_ERROR("1102",
			"用户名或密码错误"), LOGIN_USERNAME_ACCOUNT_TYPE("1103", "用户没有权限登录该APP"), LOGIN_USERNAME_LOCKED("1104",
					"您的账号已被锁定，请联系客服"), LOGIN_USERNAME_INVALID("1105",
							"您的账号已失效，请联系客服"), LOGIN_MANAGER_USERNAME_ACCOUNT_TYPE("1106", "用户没有权限登录该平台"),

	// 注册 12
	REGIST_PARAM_TYPE_INVALID("1201", "账号类型无效"), REGIST_PARAM_MOBILE_INVALID("1202",
			"手机号码无效"), REGIST_PARAM_SMSCODE_INVALID("1203", "短信验证码无效"), REGIST_PARAM_PASSWORD_INVALID("1204",
					"密码格式无效（必须为6-16位字母+数字）"), REGIST_PARAM_MOBILE_OCCUPIED("1205",
							"该手机号码已注册，请直接登录"), REGIST_PARAM_SMSCODE_TYPE_INVALID("1206",
									"短信验证码类型无效"), REGIST_PARAM_USERNAME_INVALID("1207",
											"用户名无效（必须为6-16位字母或数字）"), REGIST_PARAM_USERNAME_OCCUPIED("1208",
													"该用户名已注册，请使用其他用户名"), REGIST_PARAM_STAFF_NAME_INVALID("1209",
															"员工姓名无效"), REGIST_PARAM_ROLE_ID_INVALID("1210",
																	"请选择角色权限"), REGIST_PARAM_MEMBER_SUB_INVALID("1211",
																			"员工编码无效"),

	// 通用类
	COMMON_IMAGE_FILE_INVALID("7101", "图片文件无效"),

	OTHER_TOKEN_TIMEOUT("9000", "登录会话过期，请重新登录"), OTHER_UNAUTHORIZED("9001", "无权操作"), OTHER_APP_VERSION_NOTFOUND("9002",
			"暂未找到最新版本信息"), OTHER_WAIT_REALNAME("9003", "请先完成实名认证"), OTHER_SMSCODE_ERROR("9004",
					"短信发送失败，请稍后再试"), OTHER_EINVAL("9005",
							"传参无效"), OTHER_NOT_PUSH_ID("9006", "推送参数为空"), OTHER_ERROR("9999", "操作有误,请和管理员联系");
	// OTHER_ERROR("9999", "服务器内部错误");

	public final static String RECEIPT_NOT_AUDIT_SUCCESS = "-1";
	public final static String NO_TRANSPORT_COMPLETE = "0";
	public final static String SUCCESS_CODE = "1";
	public final static String OTHER_ERROR_CODE = "9999";

	public String code;
	public String msg;

	private RC(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

}
