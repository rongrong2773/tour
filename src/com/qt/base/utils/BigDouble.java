package com.qt.base.utils;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.UUID;

/**
 * double 数值计算或转换
 * 
 * @author 闫子扬
 * @date 2018年6月29日11:21:15
 * 
 */
public class BigDouble {

	
	/**
	 * 计算 (a/b)*c <br>
	 * 计算，将吨位和单价同时扩大10000倍，计算完还原，防止精度丢失 参数： 吨位，单价
	 */
	public static double getCount(double a, double b, double c) {
		double price_count = ((a * 100) / (b * 100) * (c * 100)) / 100;
		return price_count;
	}
	
	/**
	 * 相乘
	 * 计算，将吨位和单价同时扩大10000倍，计算完还原，防止精度丢失 参数： 吨位，单价
	 */
	public static double getPriceCount(double price, double number) {
		double price_count = ((price * 10000) * (number * 10000)) / 100000000;
		return price_count;
	}
	
	/**
	 * 相乘
	 * 计算，将吨位和单价同时扩大10000倍，计算完还原，防止精度丢失 参数： 吨位，单价
	 */
	public static double getPriceCount(double price, double number, double count) {
		double price_count = ((price * 100) * (number * 100)) * (count * 100) / 1000000;
		return price_count;
	}
	
	/**
	 * 相加。
	 * 计算，两个数都扩大10000倍。计算后还原，防止精度丢失。
	 */
	public static double getAdditionCount(double numberOne, double numberTwo){
		double count = (numberOne * 10000) + (numberTwo * 10000);
		return count/10000;
	}
	
	/**
	 * 相减。
	 * 计算，两个数都扩大10000倍。计算后还原，防止精度丢失。
	 */
	public static double getMinusCount(double numberOne, double numberTwo){
		double count = (numberOne * 10000) - (numberTwo * 10000);
		return count/10000;
	}

	/**
	 * 进位，保留小数点后两位
	 * 
	 * @param double
	 * @return
	 */
	public static double getCarryCount(double number) {
		number = number * 10000;
		BigDecimal carryCount = new BigDecimal(number).divide(new BigDecimal(10000)).setScale(2, BigDecimal.ROUND_UP);
		return carryCount.doubleValue();
	}
	
	/**
	 * 进位，取整数（无小数）
	 * 
	 * @param double
	 * @return
	 */
	public static double getCarryCount2(double number) {
		number = number * 10000;
		BigDecimal carryCount = new BigDecimal(number).divide(new BigDecimal(10000)).setScale(0, BigDecimal.ROUND_UP);
		return carryCount.doubleValue();
	}

	/**
	 * 舍位，保留小数点后两位
	 * 
	 * @param double
	 * @return
	 */
	public static double getHouseCount(double number) {
		number = number * 10000;
		BigDecimal houseCount = new BigDecimal(number).divide(new BigDecimal(10000)).setScale(2, BigDecimal.ROUND_DOWN);
		return houseCount.doubleValue();
	}

	/**
	 * 四舍五入，保留小数点后两位
	 * 
	 * @param double
	 * @return
	 */
	public static double getRoundingCount(double number) {
		BigDecimal roundingCount = new BigDecimal(number).setScale(2, BigDecimal.ROUND_HALF_UP);
		return roundingCount.doubleValue();
	}
	
	
	/**
	 * 保留小数点后2位，整数则填充.00
	 * 
	 * @param double
	 * @return
	 */
	public static String getReservedDecimal2W(double number) {
		return String.format("%.2f", number);
	}
	
	/**
	 * 保留小数点后3位，整数则填充.000
	 * 
	 * @param double
	 * @return
	 */
	public static String getReservedDecimal3W(double number) {
		return String.format("%.3f", number);
	}

	/**
	 * 去除科学计数法
	 * 
	 * @param double
	 * @return string
	 */
	public static String scientificNotation(double number) {
		NumberFormat nf = NumberFormat.getInstance();
		nf.setGroupingUsed(false);
		String num = nf.format(number);
		return num;
	}

	/**
	 * 
	 * 将银行卡中间八个字符隐藏为*
	 */
	public static String getHideBankCardNum(String bankCardNum) {
		try {
			if (bankCardNum == null)
				return "未绑定";

			int length = bankCardNum.length();

			if (length > 4) {
				String startNum = bankCardNum.substring(0, 4);
				String endNum = bankCardNum.substring(length - 4, length);
				bankCardNum = startNum + " **** **** " + endNum;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bankCardNum;
	}

	public static String getUUID() {
		return UUID.randomUUID().toString().replaceAll("\\-", "");
	}
	
	public static void main(String[] args) {

		double d = 114.2233;
		String ad = String.format("%.3f", d);
		System.out.println(ad);
	}
}
