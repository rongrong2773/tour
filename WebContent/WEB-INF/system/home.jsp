<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>首页</title>
<style type="text/css">
	.main{
		width: 700px;
		margin: 200px auto;
	}
	.ipt{
		margin-bottom: 20px;
	}
	#userList td{width:100px;}
</style>
</head>
<body>
<div class="main">
	<h2>会员账号信息</h2>
	<table id="userList">
		
	</table>
	<div class="addUserDiv">
		<h2>添加会员账号</h2>
		<div class="ipt">
			<span>账号：</span>
			<input type="text" id="username" />
		</div>
		<div class="ipt">
			<span>密码：</span>
			<input type="text" id="password" />
		</div>
		<div class="ipt">
			<span>昵称：</span>
			<input type="text" id="nickname" />
		</div>
		<div class="ipt">
			<span>状态：</span>
			<select id="sts">
				<option value="">请选择</option>
				<option value="A">正常</option>
				<option value="P">失效</option>
			</select>
		</div>
		<div>
			<button id="addUserBtn">添加</button>
		</div>
	</div>
</div>
<script type="text/javascript" src="/tour/statics/js/jquery1.11.1.js"></script>
<script type="text/javascript">
	$(function(){
		$.ajax({
			url: '/tour/system/getUserList.html',
			type: 'post',
			dataType: 'json',
			data: {},
			success: function(res){
				console.log(res)
				if(res.success){
					var data = res.data;
					var str = '<tr><td>序号</td><td>账号</td><td>昵称</td><td>账号状态</td><td>创建时间</td><td>更新</td><td>删除</td></tr>';
					for(var i=0;i<data.length;i++){
						var sts = '';
						if(data[i].sts == 'A'){
							sts = '正常';
						}else{
							sts = '失效';
						}
						str += '<tr><td class="userId">'+data[i].userId+'</td><td>'+data[i].username+'</td><td class="nickname">'+data[i].nickname+'</td><td class="sts">'+sts+'</td><td>'+data[i].createTime+'</td><td><button onclick="update(this)">更新</button></td><td><button onclick="deleteUser(this)">删除</button></td></tr>';
					}
					$('#userList').append(str)
				}
			},
			error: function(){}
		})
	})
	
	//添加
	$('#addUserBtn').click(function(){
		var username = $('#username').val();
		if(username == ''){
			alert('请填写账号')
			return;
		}
		var password = $('#password').val();
		if(password == ''){
			alert('请填写密码')
			return;
		}
		var nickname = $('#nickname').val();
		if(nickname == ''){
			alert('请填写昵称')
			return;
		}
		var sts = $('#sts').val();
		var stsText = $('#sts option:selected').text();
		if(sts == ''){
			alert('请选择账号状态')
			return;
		}
		
		var param = {'username': username, 'password': password, 'nickname': nickname, 'sts': sts};
		
		$.ajax({
			url: '/tour/system/addUser.html',
			type: 'post',
			dataType: 'json',
			data: param,
			success: function(res){
				alert(res.message)
				if(res.success){
					window.location.reload();
				}
			},
			error: function(){}
		})
	})
	
	//声明全局变量
	var nicknameHtml, stsHtml, updateBtnHtml;
	//更新, 改变标签
	function update(object){
		nicknameHtml = $(object).parent('td').siblings('.nickname').html();
		stsHtml = $(object).parent('td').siblings('.sts').html();
		updateBtnHtml = $(object).parent('td').html();
		$(object).parent('td').siblings('.nickname').html('').append('<input type="text" style="width:100px" value="'+nicknameHtml+'" />');
		var stsStr = '<select><option value="">请选择</option>';
		if(stsHtml == '正常'){
			stsStr += '<option value="A" selected>正常</option>';
			stsStr += '<option value="P">失效</option></select>';
		}else{
			stsStr += '<option value="A">正常</option>';
			stsStr += '<option value="P" selected>失效</option></select>';
		}
		
		$(object).parent('td').siblings('.sts').html('').append(stsStr);
		$(object).parent('td').html('').append('<button onclick="updateUser(this)">确定</button><button onclick="updateCancel(this)">取消</button>');
	}
	
	//取消更新, 还原标签
	function updateCancel(object){
		$(object).parent('td').siblings('.nickname').html('').append(nicknameHtml);
		$(object).parent('td').siblings('.sts').html('').append(stsHtml);
		$(object).parent('td').html('').append(updateBtnHtml);
	}
	
	//更新数据
	function updateUser(object){
		var userId = $(object).parent('td').siblings('.userId').text();
		var nickname = $(object).parent('td').siblings('.nickname').children().val();
		if(nickname == ''){
			alert('请填写昵称')
			return;
		}
		var sts = $(object).parent('td').siblings('.sts').children().val();
		if(sts == ''){
			alert('请选择账号状态')
			return;
		}
		var stsText = $(object).parent('td').siblings('.sts').find('select option:selected').text();
		var param = {'userId': userId, 'nickname': nickname, 'sts': sts};
		
		$.ajax({
			url: '/tour/system/updateUser.html',
			type: 'post',
			dataType: 'json',
			data: param,
			success: function(res){
				alert(res.message)
				if(res.success){
					$(object).parent('td').siblings('.nickname').html('').html(nickname);
					$(object).parent('td').siblings('.sts').html('').html(stsText);
					$(object).parent('td').html('').append(updateBtnHtml);
				}
			},
			error: function(){}
		})
	}
	
	function deleteUser(object){
		var userId = $(object).parent('td').siblings('.userId').text();
		if (confirm("是否确认删除？")) {
			$.ajax({
				url: '/tour/system/deleteUser.html',
				type: 'post',
				dataType: 'json',
				data: {'userId': userId},
				success: function(res){
					alert(res.message)
					if(res.success){
						$(object).parents('tr').remove();
					}
				},
				error: function(){}
			})
		}
	}
		
	
</script>
</body>
</html>