<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>登录</title>
<style type="text/css">
	.main{
		width: 200px;
		margin: 200px auto;
	}
	.ipt{
		margin-bottom: 20px;
	}
</style>
</head>
<body>
<div class="main">
	<h2>hello word</h2>
	<div class="ipt">
		<span>账号：</span>
		<input type="text" id="username" />
	</div>
	<div class="ipt">
		<span>密码：</span>
		<input type="password" id="password" />
	</div>
	<div>
		<button id="btn">登陆</button>
	</div>
</div>
<script type="text/javascript" src="/tour/statics/js/jquery1.11.1.js"></script>
<script type="text/javascript">
	$('#btn').click(function(){
		var username = $('#username').val();
		var password = $('#password').val();
		var param = {'username': username, 'password': password};
		$.ajax({
			url: '/tour/system/login.html',
			type: 'post',
			dataType: 'json',
			data: param,
			success: function(res){
				alert(res.message)
				if(res.success){
					location.href = '/tour/system/gotoHome.html';
				}
			},
			error: function(){}
		})
	})
</script>
</body>
</html>